module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '75e6dce5f246b458b57f8fc00cb35405'),
  },
});
